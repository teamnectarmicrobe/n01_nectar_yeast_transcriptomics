#!/usr/bin/env python3

import numpy as np
import pandas as pd
import tensorqtl
from tensorqtl import cis

input_dir = "/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/output/03_formatted_inputs/"
output_dir = "/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/output/04_eqtls/"

phenotype_bed_file = input_dir + "GE_counts_filtered_invnorm.tensorQTL.bed.gz"
covariates_file = input_dir + "covariates_filtered_PCs_numeric.tensorQTL.txt"
genotype_file = input_dir + "SNPs_genotypes_filtered.tensorQTL.txt"
variant_file = input_dir + "SNPs_variants_filtered.tensorQTL.txt"
interaction_file = input_dir + "interaction_terms.tensorQTL.txt"

# reformat genotype and variant information
genotype_df = pd.read_csv(genotype_file, sep='\t')
genotype_df.columns.name = 'iid'
genotype_df.rename(columns={'variant':'snp'}, inplace=True)
genotype_df.set_index('snp', inplace=True)
genotype_df.head()

variant_df = pd.read_csv(variant_file, sep='\t')
variant_df.rename(columns={'variant':'snp'}, inplace=True)
variant_df.set_index('snp', inplace=True)
variant_df.head()

# load phenotypes
phenotype_df, phenotype_pos_df = tensorqtl.read_phenotype_bed(phenotype_bed_file)
phenotype_df.head()
phenotype_pos_df.head()

print("Scaffolds with expression:")
phenotype_pos_df.chr.unique()

# drop genes on scaffolds that are not genotyped
no_genotypes = phenotype_pos_df.chr.unique()[np.in1d(phenotype_pos_df.chr.unique(), variant_df.chrom.unique(), invert=True)]
genes_to_drop = phenotype_pos_df[phenotype_pos_df['chr'].isin(no_genotypes)].index.tolist()
phenotype_df.drop(genes_to_drop, inplace=True)
phenotype_pos_df.drop(genes_to_drop, inplace=True)

# load covariates
covariates_df = pd.read_csv(covariates_file, sep='\t', index_col=0) 
interaction_df = pd.read_csv(interaction_file, sep='\t', index_col=0)
# split covariates into interaction and covariate sets (clean this up later)
# interaction_df = covariates_df[['treatmentHigh']]
# covariates_df = covariates_df.drop('treatmentHigh', axis = 1)
covariates_df.head()
interaction_df.head()

# drop unwanted covariates
print("Dropping non-PC covariates")
covariates_df = covariates_df.drop(['strainNum','subpopNum','site_id','plateNum'], axis=1, errors='ignore')

# chroms = ["sc0000004", "sc0000010", "sc0000007", "sc0000008", "sc0000002", "sc0000005", "sc0000003", "sc0000006", "sc0000009", "sc0000012", "sc0000076", "sc0000062", "sc0000050", "sc0000032", "sc0000090"]
chroms = phenotype_pos_df['chr'].unique()

for testing_chrom in chroms:
    # testing_chrom = "sc0000007"
    print("Testing chromosome " + str(testing_chrom))
    phenotype_pos_df_test = phenotype_pos_df[phenotype_pos_df['chr'] == testing_chrom]
    phenotype_df_test = phenotype_df[phenotype_df.index.isin(phenotype_pos_df_test.index)]
    variant_df_test = variant_df[variant_df['chrom'] == testing_chrom]
    genotype_df_test = genotype_df[genotype_df.index.isin(variant_df_test.index)]

    cis_x_df = cis.map_nominal(genotype_df_test, variant_df_test, phenotype_df_test, phenotype_pos_df_test, 
                            prefix = output_dir + "PCs_only/jgi_gxe_eqtls.33PCs.test." + testing_chrom, 
                            covariates_df=covariates_df, interaction_df=interaction_df, maf_threshold_interaction=0.05,
                            run_eigenmt=True)

# call QTLs
# cis_df = cis.map_cis(genotype_df, variant_df, phenotype_df, phenotype_pos_df, covariates_df)
# tensorqtl.calculate_qvalues(cis_df, qvalue_lambda=0.85)

# pd.write_tsv(cis_df, "jgi_eqtls.noPC.txt", sep = "\t")

# call interaction QTLs
# cis_x_df = cis.map_nominal(genotype_df, variant_df, phenotype_df, phenotype_pos_df, prefix = output_dir + "jgi_gxe_eqtls.withPCs", 
#                            covariates_df=covariates_df, interaction_df=interaction_df, maf_threshold_interaction=0.05,
#                            run_eigenmt=True)

