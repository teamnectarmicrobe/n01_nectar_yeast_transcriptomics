#!/bin/bash
#SBATCH --job-name=tensorQTL
#SBATCH --cpus-per-task=1
#SBATCH --partition=batch
#SBATCH --account=smontgom
#SBATCH --time=5:00:00
#SBATCH --mem=300G
#SBATCH --output="/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/jobs/%j.out"
#SBATCH --error="/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/jobs/%j.err"

# ml R/4.0.3
# export R_HOME=/scg/apps/software/r/4.0.3

/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/code/run_tensorQTL.py

