indir="/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/data/"
savedir="/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/output/01_format_inputs/"
# if(!exists('counts_renamed_annot')){
#   counts_renamed_annot = fread(paste0(indir,"GE_raw_counts.txt"))
# } else {
#   cat("\nUsing existing `counts_renamed_annot` object.\n")
# }
#
# if(!exists('tpm_renamed_annot')){
#   tpm_renamed_annot = fread(paste0(savedir,"GE_TPM.txt"))
# } else {
#   cat("\nUsing existing `tpm_renamed_annot` object.\n")
# }
covar <- read_csv(paste0(indir,"covariates.csv"), col_names = F, show_col_types = FALSE)
indv <- scan(paste0(indir,"Dhami_et_al_final_SNPset.vcf.012.indv"), what = character())
head(covar)
head(indv)
covar_t = t(covar) %>%
as.data.frame() %>%
janitor::row_to_names(row_number=1)
head(covar_t)
# do we have identical entries?
covar_t %>%
group_by(sample, strain, treatment) %>%
summarize(n=n()) %>%
filter(n > 1)
# we can filter duplicate rows with distinct() later on
# do we have identical samples?
## get names of samples that appear more than once and are not duplicate rows
repeat_ids = covar_t %>%
distinct() %>%
group_by(sample) %>%
summarize(n=n()) %>%
filter(n > 1) %>%
pull(sample)
## filter to duplicate samples and collapse to the associated unique strains and conditions
repeat_ids_data = covar_t %>%
filter(sample %in% repeat_ids) %>%
group_by(sample) %>%
summarize(strains = paste0(unique(strain), collapse = ", "),
treatment = paste0(unique(treatment), collapse = ", "))
repeat_ids_data
# split repeat_ids by strain
repeat_ids_VY0351 = repeat_ids_data %>%
filter(str_detect(strains, "VY0351")) %>%
pull(sample)
repeat_ids_VY0662 = repeat_ids_data %>%
filter(str_detect(strains, "VY0662")) %>%
pull(sample)
# create final cleaned covariates
covar_clean = covar_t %>%
mutate(strain = ifelse(sample %in% repeat_ids_VY0351, "VY0351",
ifelse(sample %in% repeat_ids_VY0662, "VY0662", strain)
)
) %>%
distinct()
# clean up environment
rm(repeat_ids, repeat_ids_VY0351, repeat_ids_VY0662, repeat_ids_data)
strains <- gsub("-.*", "", indv)
head(covar_clean)
Q1 = tpm_renamed %>%
gather(key = "sample", value = "tpm", -GeneID) %>%
pull(tpm) %>%
quantile(., probs = c(0.25), na.rm=T)
low_exp = tpm_renamed %>%
gather(key = "sample", value = "tpm", -GeneID) %>%
group_by(sample) %>%
summarize(med_tpm = median(tpm, na.rm=T),
max_tpm = max(tpm, na.rm=T),
max_gene = paste0(GeneID[tpm == max(tpm, na.rm=T)], collapse = ", ")) %>%
arrange(med_tpm) %>%
filter(med_tpm <= Q1) %>%
left_join(covar_clean) %>%
dplyr::select(sample, strain, treatment, med_tpm, max_tpm, max_gene)
low_exp_strains = low_exp %>% pull(strain) %>% unique()
low_exp
# how well distributed are counts across strains?
total_to_plot = counts_renamed %>%
gather(key = "sample", value = "counts", -GeneID) %>%
group_by(sample) %>%
summarize(total_counts = sum(counts, na.rm=T)) %>%
left_join(covar_clean) %>%
arrange(total_counts) %>%
mutate(strain_group = ifelse(strain %in% low_exp_strains, strain, "All other strains")) %>%
filter(!is.na(strain))
total_to_plot %>%
ggplot() +
geom_density(aes(x=total_counts, color=strain, fill=strain), alpha = 0.4) +
theme_bw(base_size = 12) +
facet_wrap( vars(strain_group), scales = "free_y") +
scale_x_continuous(labels = scales::comma) +
theme_bw() +
theme(legend.position = "none")
total_to_plot %>%
ggplot() +
geom_histogram(aes(x=total_counts, color=strain_group, fill=strain_group), alpha = 0.4) +
theme_bw(base_size = 12) +
scale_color_manual(values = c("grey37", "red")) +
scale_fill_manual(values = c("grey37", "red")) +
# facet_wrap( vars(strain_group), scales = "free_y") +
scale_x_continuous(labels = scales::comma) +
theme_bw() #+
# theme(legend.position = "none")
knitr::opts_chunk$set(echo = TRUE)
### Packages
# data load-in:
library(readr)
library(data.table)
library(Rgb) # read.gtf
# data organization:
library(janitor) # row_to_names
library(dplyr)
library(tidyr)
library(tibble) # column_to_rownames
library(stringr)
# data processing:
library(PCAtools) # pca (installation: BiocManager::install("PCAtools"))
library(RNOmni) # RankNorm (installation: BiocManager::install("RNOmni"))
# plotting:
library(ggplot2)
# utility:
`%notin%` = Negate(`%in%`)
indir="/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/output/02_harmonized_data/"
savedir="/oak/stanford/groups/fukamit/callie_chappell/jgi_transcriptome/eQTL/output/03_formatted_inputs/"
dir.create(file.path(savedir), showWarnings = FALSE)
# load data
# Covariates
if(!exists('covar_clean_filt')){
covar_clean_filt = fread(paste0(indir,"covariates_complete_substrain_clean_nodupes_filtered.txt"), header=TRUE)
} else {
cat("\nUsing existing `covar_clean_filt` object.\n")
}
# Expression
if(!exists('counts_filt')){
counts_filt = fread(paste0(indir,"GE_counts_withTSS_filtered.txt"), header=TRUE)
} else {
cat("\nUsing existing `counts_filt` object.\n")
}
if(!exists('tpm_filt')){
tpm_filt = fread(paste0(indir,"GE_TPM_withTSS_filtered.txt"), header=TRUE)
} else {
cat("\nUsing existing `tpm_filt` object.\n")
}
# Genotypes
if(!exists('pos_filt')){
pos_filt = fread(paste0(indir,"SNPs_variants_filtered.txt"), header=TRUE)
} else {
cat("\nUsing existing `pos_filt` object.\n")
}
if(!exists('snps_t_filt')){
snps_t_filt = fread(paste0(indir,"SNPs_genotypes_filtered.txt"), header=TRUE)
} else {
cat("\nUsing existing `snps_t_filt` object.\n")
}
# to make the matrix numeric, we must remove the non-numeric
# columns from the matrix body; to do this, we set the GeneID
# as the rownames and remove all other non-numeric annotations
# tpm_mat = tpm_filt %>%
#   dplyr::select(-chr, -start, -end) %>%
#   column_to_rownames("phenotype_id") %>%
#   as.matrix()
#
# # verify that the matrix is indeed numeric now
# is.numeric(tpm_mat)
# tpm_mat[1:5,1:3]
counts_mat = counts_filt %>%
dplyr::select(-chr, -start, -end) %>%
column_to_rownames("phenotype_id") %>%
as.matrix()
# verify that the matrix is indeed numeric now
is.numeric(counts_mat)
counts_mat[1:5,1:3]
# normalize counts per gene across samples using edger
library(edgeR)
cpm_mat = cpm(counts_mat, normalized.lib.sizes=TRUE)
# normalize counts using inverse normal transform
cpm_inv_norm <- apply(cpm_mat, 1, RankNorm) %>% t()
cpm_inv_norm[1:5,1:3]
# normalize tpms using inverse normal transform
tpm_inv_norm <- apply(tpm_mat, 1, RankNorm) %>% t()
# and visualize the normalized expression
plot_matrix_distribution <- function(m, variable, title, log_transform = F, log_buffer=1){
if(log_transform){
# print(paste0("Adding ", log_buffer, " to ", variable, " prior to log-scaling axis"))
buffered_variable = paste0(variable, "+", log_buffer)
} else {
buffered_variable = variable
}
p = m %>%
as.data.frame() %>%
rownames_to_column(var = "phenotype_id") %>%
gather(key = "sample", value = !!variable, -phenotype_id) %>%
ggplot() +
geom_density(aes_string(x = buffered_variable, color = "sample"), alpha = 0.5) +
# scale_x_continuous(trans = "log10") +
theme_bw(base_size = 12) +
theme(legend.position = "none") +
ggtitle("Raw Counts")
if(log_transform){
p = p + scale_x_continuous(trans = "log10", labels = scales::comma)
}
print(p)
}
plot_matrix_distribution(counts_mat, "counts", "Raw Counts", log_transform=T, log_buffer=1)
# plot_matrix_distribution(tpm_mat, "tpm", "Raw TPM", log_transform=T, log_buffer=0.1)
plot_matrix_distribution(cpm_inv_norm, "inv_norm_cpm", "Inverse normalized Counts Per Million")
# plot_matrix_distribution(tpm_inv_norm, "inv_norm_tpm", "Inverse normalized TPM")
cpm_inv_norm_annot = counts_filt %>%
dplyr::select(chr, start, end, phenotype_id) %>%
left_join(cpm_inv_norm %>%
as.data.frame() %>%
rownames_to_column(var = "phenotype_id"))
cpm_inv_norm_annot[1:5,1:7]
# save as bed file
cpm_inv_norm_annot %>%
rename("#chr" = chr) %>%
write_delim(., file=paste0(savedir,"GE_counts_filtered_invnorm.tensorQTL.bed.gz"), delim = "\t")
# get PCs
# input is a numeric matrix; Variables are expected to be in the rows and samples in the columns by default.
pca_cpmint <- pca(cpm_inv_norm)
screeplot(pca_cpmint, components = getComponents(pca_cpmint)[1:50], hline = 80, axisLabSize = 18, titleLabSize = 22)
# pca_tpmint <- pca(tpm_inv_norm)
# screeplot(pca_tpmint, components = getComponents(pca_tpmint)[1:50], hline = 80, axisLabSize = 18, titleLabSize = 22)
#
# pca_counts <- pca(counts_mat)
# screeplot(pca_counts, components = getComponents(pca_counts)[1:50], hline = 80, axisLabSize = 18, titleLabSize = 22)
#
# pca_tpm <- pca(tpm_mat)
# screeplot(pca_tpm, components = getComponents(pca_tpm)[1:50], hline = 80, axisLabSize = 18, titleLabSize = 22)
plot_pca = function(pc_res, title, colorby = "subpopulation"){
p = pc_res$rotated %>%
rownames_to_column(var = "sample") %>%
left_join(covar_clean_filt) %>%
ggplot(aes_string(x="PC1", y="PC2", color = as.factor(colorby), shape = "treatment")) +
geom_point(size = 2, alpha = 0.8) +
scale_shape_manual(values = c(16, 24)) +
ggtitle(title)
print(p)
}
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "subpopulation")
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "site_id")
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "plate")
# plot_pca(pca_tpmint, "Inverse normal TPM")
# plot_pca(pca_counts, "Raw Counts")
# plot_pca(pca_tpm, "Raw TPM")
plot_pca = function(pc_res, title, colorby = "subpopulation"){
p = pc_res$rotated %>%
rownames_to_column(var = "sample") %>%
left_join(covar_clean_filt) %>%
ggplot(aes_string(x="PC1", y="PC2",
color = paste0("as.factor(",colorby,")"), shape = "treatment")) +
geom_point(size = 2, alpha = 0.8) +
scale_shape_manual(values = c(16, 24)) +
ggtitle(title)
print(p)
}
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "subpopulation")
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "site_id")
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "plate")
# plot_pca(pca_tpmint, "Inverse normal TPM")
# plot_pca(pca_counts, "Raw Counts")
# plot_pca(pca_tpm, "Raw TPM")
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "subpopulation")
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "site_id")
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "plate")
plot_pca(pca_cpmint, "Inverse normal Counts per million", colorby = "strain")
# determine number of recommended PCs using GavishDonoho method
gv_res <- chooseGavishDonoho(cpm_inv_norm, var.explained=pca_cpmint$sdev^2, noise=1)
rec_pc_count = gv_res[[1]]
print(paste("Recommended number of PCs, based on Gavish Donoho method:", rec_pc_count))
# subset to recommended number of PCs
sig_pcs <- pca_cpmint$rotated[, 1:rec_pc_count, drop=FALSE] %>%
as.data.frame() %>%
rownames_to_column(var = "sample")
dim(sig_pcs)
head(sig_pcs)
# create interaction data frame
interaction_df = covar_clean_filt %>%
dplyr::select(sample, treatment) %>%
# make numeric
mutate(treatmentHigh = as.numeric(treatment == "high"))
head(interaction_df)
# remove interaction term from covariates
covar_clean_filt_noX = covar_clean_filt %>% dplyr::select(-treatment)
head(covar_clean_filt_noX)
names(covar_clean_filt_noX)
covar_clean_filt_noX_PCs = covar_clean_filt_noX %>%
# add PCs
left_join(sig_pcs) %>%
# re-merge strain & substrain
unite("strain", c(strain, substrain), na.rm = TRUE) %>%
dplyr::select(-covariate_strain, -metadata_strain, -rna_concentration,
-site_id, -latitude, -longitude)
View(covar_clean_filt_noX_PCs)
covar_clean_filt_noX_PCs = covar_clean_filt_noX %>%
# add PCs
left_join(sig_pcs) %>%
# re-merge strain & substrain
unite("strain", c(strain, substrain), na.rm = TRUE) %>%
dplyr::select(-covariate_strain, -metadata_strain, -rna_concentration,
-site, -latitude, -longitude)
covar_clean_filt_noX_PCs = covar_clean_filt_noX %>%
# add PCs
left_join(sig_pcs) %>%
dplyr::select(-covariate_strain, -metadata_strain, -rna_concentration,
-site, -latitude, -longitude)
# convert categorical covariates to numeric
covar_clean_filt_num = covar_clean_filt_noX_PCs %>%
# re-merge strain & substrain
unite("strain", c(strain, substrain), na.rm = TRUE) %>%
# make variables numeric
mutate(treatmentHigh = as.numeric(treatment == "high")) %>%
mutate(subpopNum = ifelse(subpopulation == "Group3", 3,
ifelse(subpopulation == "Group2", 2,
ifelse(subpopulation == "Group1", 1, 0))))
# convert categorical covariates to numeric
covar_clean_filt_num = covar_clean_filt_noX_PCs %>%
# re-merge strain & substrain
unite("strain", c(strain, substrain), na.rm = TRUE) %>%
# make variables numeric
mutate(subpopNum = ifelse(subpopulation == "Group3", 3,
ifelse(subpopulation == "Group2", 2,
ifelse(subpopulation == "Group1", 1, 0))))
covar_clean_filt_num$strainNum = group_indices(covar_clean_filt_num, strain)
# save as a map
# write.table(covar_clean_filt_num, file=paste0(savedir,"covariate_map.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
# subset to numeric variables
covar_clean_filt_num = covar_clean_filt_num %>%
select(sample, strainNum, treatmentHigh, subpopNum, starts_with("PC"))
# convert categorical covariates to numeric
covar_clean_filt_num = covar_clean_filt_noX_PCs %>%
# re-merge strain & substrain
unite("strain", c(strain, substrain), na.rm = TRUE) %>%
# make variables numeric
mutate(subpopNum = ifelse(subpopulation == "Group3", 3,
ifelse(subpopulation == "Group2", 2,
ifelse(subpopulation == "Group1", 1, 0))))
covar_clean_filt_num$strainNum = group_indices(covar_clean_filt_num, strain)
# save as a map
# write.table(covar_clean_filt_num, file=paste0(savedir,"covariate_map.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
# subset to numeric variables
covar_clean_filt_num = covar_clean_filt_num %>%
select(sample, strainNum, subpopNum, starts_with("PC"))
# rename to match order of phenotype data
covar_clean_filt_num = covar_clean_filt_num[match(names(counts_filt)[-c(1:4)], covar_clean_filt_num$sample),]
covar_clean_filt_num[1:5,]
covar_clean_filt_noX_PCs[5:10,1:5]
# convert categorical covariates to numeric
covar_clean_filt_num = covar_clean_filt_noX_PCs %>%
# re-merge strain & substrain
unite("strain", c(strain, substrain), na.rm = TRUE) %>%
# make variables numeric
mutate(subpopNum = ifelse(subpopulation == "Group3", 3,
ifelse(subpopulation == "Group2", 2,
ifelse(subpopulation == "Group1", 1, 0))))
covar_clean_filt_num$strainNum = group_indices(covar_clean_filt_num, strain)
# save as a map
# write.table(covar_clean_filt_num, file=paste0(savedir,"covariate_map.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
# subset to numeric variables
covar_clean_filt_num = covar_clean_filt_num %>%
select(sample, strainNum, subpopNum, site_id, starts_with("PC"))
# rename to match order of phenotype data
covar_clean_filt_num = covar_clean_filt_num[match(names(counts_filt)[-c(1:4)], covar_clean_filt_num$sample),]
covar_clean_filt_num[1:5,]
covar_clean_filt_noX_PCs[5:10,1:5]
covar_clean_filt_noX_PCs = covar_clean_filt_noX %>%
# add PCs
left_join(sig_pcs) %>%
# re-merge strain & substrain
unite("strain", c(strain, substrain), na.rm = TRUE) %>%
# drop unneeded columns
dplyr::select(-covariate_strain, -metadata_strain, -rna_concentration,
-site, -latitude, -longitude)
covar_clean_filt_noX_PCs = covar_clean_filt_noX %>%
# add PCs
left_join(sig_pcs) %>%
# re-merge strain & substrain
unite("strain", c(strain, substrain), na.rm = TRUE) %>%
# drop unneeded columns
dplyr::select(-covariate_strain, -metadata_strain, -rna_concentration,
-latitude, -longitude)
covar_clean_filt_num_t = t(covar_clean_filt_num)
covar_clean_filt_num_t[, 1:5]
covar_clean_filt_num_t[1:5, 1:5]
write.table(interaction_df, file=paste0(savedir,"interaction.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_noX_PCs, file=paste0(savedir,"covar_clean_filt_noX_PCs.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_num, file=paste0(savedir,"covar_clean_filt_num.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_num_t, file=paste0(savedir,"covar_clean_filt_num_t.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
## change back to write.table and included quote=F
# save in dataframe format (samples x covariates) for tensorQTL python script
# write.table(covar_clean_filt_num, file=paste0(savedir,"covariates.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
# save in tensorQTL text file format (covariates x samples) for commandline tensorQTL
# write.table(covar_clean_filt_num_t, file=paste0(savedir,"covariates_transposed.txt"), sep = "\t", row.names=TRUE, col.names=FALSE, quote=FALSE)
write.table(interaction_df, file=paste0(savedir,"interaction.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_noX_PCs, file=paste0(savedir,"covariates_filtered_PCs.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_num, file=paste0(savedir,"covariates_filtered_PCs_numeric.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_num_t, file=paste0(savedir,"covariates_filtered_PCs_numeric_transposed.tensorQTL.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
## change back to write.table and included quote=F
# save in dataframe format (samples x covariates) for tensorQTL python script
# write.table(covar_clean_filt_num, file=paste0(savedir,"covariates.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
# save in tensorQTL text file format (covariates x samples) for commandline tensorQTL
# write.table(covar_clean_filt_num_t, file=paste0(savedir,"covariates_transposed.txt"), sep = "\t", row.names=TRUE, col.names=FALSE, quote=FALSE)
# ensure order matches
interaction_df = interaction_df[match(names(counts_filt)[-c(1:4)], interaction_df$sample),]
# ensure order matches
interaction_df = interaction_df[match(names(cpm_inv_norm_annot)[-c(1:4)], interaction_df$sample),]
write.table(interaction_df, file=paste0(savedir,"interaction_terms.tensorQTL.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_noX_PCs, file=paste0(savedir,"covariates_filtered_PCs.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_num, file=paste0(savedir,"covariates_filtered_PCs_numeric.tensorQTL.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
write.table(covar_clean_filt_num_t, file=paste0(savedir,"covariates_filtered_PCs_numeric_transposed.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
## change back to write.table and included quote=F
# save in dataframe format (samples x covariates) for tensorQTL python script
# write.table(covar_clean_filt_num, file=paste0(savedir,"covariates.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
# save in tensorQTL text file format (covariates x samples) for commandline tensorQTL
# write.table(covar_clean_filt_num_t, file=paste0(savedir,"covariates_transposed.txt"), sep = "\t", row.names=TRUE, col.names=FALSE, quote=FALSE)
# create interaction data frame
interaction_df = covar_clean_filt %>%
dplyr::select(sample, treatment) %>%
# make numeric
mutate(treatmentHigh = as.numeric(treatment == "high")) %>%
dplyr::select(-treatment)
head(interaction_df)
# remove interaction term from covariates
covar_clean_filt_noX = covar_clean_filt %>% dplyr::select(-treatment)
head(covar_clean_filt_noX)
# ensure order matches
interaction_df = interaction_df[match(names(cpm_inv_norm_annot)[-c(1:4)], interaction_df$sample),]
write.table(interaction_df, file=paste0(savedir,"interaction_terms.tensorQTL.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
scaffolds
cpm_inv_norm_annot %>% pull(chr) %>% unique()
View(covar_clean_filt_num)
library(corrr)
install.packages("corrr")  # though keep eye out for new version coming soon
library(corrr)
covar_clean_filt_num %>% correlate() %>% focus(strainNum:PC33)
covar_clean_filt_num %>% correlate()
covar_clean_filt_num %>% correlate() %>% column_to_rownames(term)
covar_clean_filt_num %>% correlate() %>% column_to_rownames("term")
covar_corr = covar_clean_filt_num %>%
correlate() %>%
column_to_rownames("term") %>%
as.matrix()
covar_corr
corrplot(covar_corr, method = 'number') # colorful number
library(corrplot)
corrplot(covar_corr, method = 'number') # colorful number
corrplot(covar_corr, method = 'number') # colorful number
corrplot(covar_corr) # , method = 'number'colorful number
corrplot.mixed(covar_corr, diag = FALSE) # colorful number
corrplot.mixed(covar_corr, order = 'AOE')
covar_corr
diag(covar_corr) <- 1
covar_corr
covar_corr[1:5,1:5]
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE) # colorful number
# convert categorical covariates to numeric
covar_clean_filt_num = covar_clean_filt_noX_PCs %>%
# make variables numeric
mutate(subpopNum = ifelse(subpopulation == "Group3", 3,
ifelse(subpopulation == "Group2", 2,
ifelse(subpopulation == "Group1", 1, 0))))
covar_clean_filt_num$strainNum = group_indices(covar_clean_filt_num, strain)
covar_clean_filt_num$plateNum = group_indices(covar_clean_filt_num, plate)
# save as a map
# write.table(covar_clean_filt_num, file=paste0(savedir,"covariate_map.txt"), sep = "\t", row.names=FALSE, quote=FALSE)
# subset to numeric variables
covar_clean_filt_num = covar_clean_filt_num %>%
select(sample, strainNum, subpopNum, site_id, plateNum, starts_with("PC"))
# rename to match order of phenotype data
covar_clean_filt_num = covar_clean_filt_num[match(names(cpm_inv_norm_annot)[-c(1:4)], covar_clean_filt_num$sample),]
covar_clean_filt_num[1:5,]
covar_clean_filt_noX_PCs[5:10,1:5]
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE, order="hclust", tl.cex = 10) # colorful number
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE, order="hclust", tl.cex = 1) # colorful number
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE, order="hclust", tl.cex = 0.5) # colorful number
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE, order="hclust", tl.cex = 1, cl.cex = 0.5) # colorful number
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE, order="hclust",
tl.cex = 1, cl.cex = 1.5, number.cex = 0.5) # colorful number
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE, order="hclust",
tl.cex = 1, cl.cex = 1.5, number.cex = 0.75) # colorful number
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE, order = 'AOE',
tl.cex = 1, cl.cex = 1.5, number.cex = 0.75) # colorful number
corrplot.mixed(covar_corr, order = 'AOE')
corrplot.mixed(covar_corr, order = 'AOE', tl.cex = 0.5)
range(covar_corr)
range(abs(covar_corr))
quantiles(abs(covar_corr))
quantile(abs(covar_corr))
covar_corr_top = covar_corr[covar_corr >= 0.1]
dim(covar_corr_top)
covar_corr[covar_corr >= 0.1]
covar_corr[covar_corr >= 0.1, covar_corr >= 0.1]
covar_corr[covar_corr >= 0.1,]
covar_corr
corrplot.mixed(covar_corr, order = 'AOE', tl.cex = 0.5)
corrplot.mixed(covar_corr, order = 'AOE', tl.cex = 0.5, number.cex = 0.75)
corrplot.mixed(covar_corr, order = 'AOE', tl.cex = 0.5, number.cex = 0.5)
corrplot.mixed(covar_corr, tl.cex = 0.5, number.cex = 0.5)
# install.packages("corrr")  # though keep eye out for new version coming soon
library(corrr)
library(corrplot)
# get correlation between all covariates
covar_corr = covar_clean_filt_num %>%
correlate() %>%
column_to_rownames("term") %>%
as.matrix()
diag(covar_corr) <- 1
covar_corr[1:5,1:5]
# plot correlation
corrplot(covar_corr, method = 'number', type = 'lower', diag = FALSE, order = 'AOE',
tl.cex = 1, cl.cex = 1.5, number.cex = 0.75) # colorful number
corrplot.mixed(covar_corr, tl.cex = 0.5, number.cex = 0.5)
