#!/bin/bash 

#BBTools version 38.25 

#The steps below recapitulate the output of RQCFilter2 when run like this: 

#rqcfilter2.sh in=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz path=79 rna=t minlength=49 qtrim=r maq=10 trimq=6 trimfragadapter=t phix=t maxns=1 mlf=0.33 ribomap=t removehuman=t removedog=t removecat=t removemouse=t khist=t removemicrobes=t sketch kapa=t taxlist=27327 clumpify=t tmpdir= barcodefilter=f trimpolyg=5 usejni=f 

#Data dependencies are available at http://portal.nersc.gov/dna/microbial/assembly/bushnell/RQCFilterData.tar 





sendsketch.sh in=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz out=sketch.txt reads=2000000 name=Metschnikowia reukaufii fname=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz minprob=0.2 samplerate=1.0 merge printname0=f records=20 color=false depth depth2 unique2 volume sortbyvolume contam2=genus nt ow 



sendsketch.sh in=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz out=sketch.txt reads=2000000 name=Metschnikowia reukaufii fname=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz minprob=0.2 samplerate=1.0 merge printname0=f records=20 color=false depth depth2 unique2 volume sortbyvolume contam2=genus refseq append 



sendsketch.sh in=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz out=sketch.txt reads=2000000 name=Metschnikowia reukaufii fname=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz minprob=0.2 samplerate=1.0 merge printname0=f records=20 color=false depth depth2 unique2 volume sortbyvolume contam2=genus silva local minkeycount=2 append 



bbmerge.sh overwrite=true in1=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz outa=adaptersDetected.fa reads=1m 



clumpify.sh pigz=t unpigz=t zl=4 reorder in1=12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz out1=TEMP_CLUMP_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz passes=1 



bbduk.sh ktrim=r ordered minlen=49 minlenfraction=0.33 mink=11 tbo tpe rcomp=f overwrite=true k=23 hdist=1 hdist2=1 ftm=5 pratio=G,C plen=20 phist=phist.txt qhist=qhist.txt bhist=bhist.txt gchist=gchist.txt pigz=t unpigz=t zl=4 ow=true in1=TEMP_CLUMP_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz out1=TEMP_TRIM_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz rqc=hashmap outduk=ktrim_kmerStats1.txt stats=ktrim_scaffoldStats1.txt loglog loglogout adapters=adaptersDetected.fa 



rm TEMP_CLUMP_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz 



seal.sh ordered overwrite=true k=31 hdist=0 pigz=t unpigz=t zl=6 in1=TEMP_TRIM_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outu1=TEMP_SPIKEIN_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outm=spikein.fq.gz stats=scaffoldStatsSpikein.txt loglog loglogout ref=kapatags.L40.fa.gz 



rm TEMP_TRIM_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz 



bbduk.sh maq=10.0,0 trimq=6.0 qtrim=r ordered overwrite=true maxns=1 minlen=49 minlenfraction=0.33 k=31 hdist=1 pigz=t unpigz=t zl=6 cf=t barcodefilter=f ow=true in1=TEMP_SPIKEIN_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz out1=TEMP_FILTER1_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outm=synth1.fq.gz rqc=hashmap outduk=kmerStats1.txt stats=scaffoldStats1.txt trimpolygleft=5 trimpolygright=5 loglog loglogout ref=pJET1.2.fa.gz 



rm TEMP_SPIKEIN_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz 



bbduk.sh ordered overwrite=true k=20 hdist=1 pigz=t unpigz=t zl=6 ow=true in1=TEMP_FILTER1_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz out1=TEMP_FILTER2_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outm=synth2.fq.gz outduk=kmerStats2.txt stats=scaffoldStats2.txt loglog loglogout ref=short.fa.gz 



rm TEMP_FILTER1_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz 



filterbytaxa.sh names=27327 tree=tree.taxtree.gz level=order in=fusedERPBBmasked2.fa.gz out=TEMP_TAXA_1a1503228d98e2bf267bbb3872495_taxa.fa.gz ow=true append=false include=false besteffort=false results=microbesUsed.txt 



bbmap.sh ordered quickmatch k=13 idtag=t printunmappedcount ow=true qtrim=rl trimq=10 untrim null ref=TEMP_TAXA_1a1503228d98e2bf267bbb3872495_taxa.fa.gz pigz=t unpigz=t zl=6 minid=.95 idfilter=.95 maxindel=3 minhits=2 bw=12 bwr=0.16 null maxsites2=10 build=1 tipsearch=0 in1=TEMP_FILTER2_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outu1=TEMP_MICROBE_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outm=microbes.fq.gz scafstats=commonMicrobes.txt 



rm TEMP_TAXA_1a1503228d98e2bf267bbb3872495_taxa.fa.gz 



rm TEMP_FILTER2_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz 



filterbytaxa.sh names=27327 tree=tree.taxtree.gz level=species in=SSU_LSU_sorted.fa.gz out=TEMP_TAXA_1a1503228d98e2bf267bbb3872495_chloroMitoRiboRef.fa.gz ow=true append=false include=true besteffort=true 



bbmap.sh ordered minratio=.9 maxindel=3 null minhits=2 tipsearch=4 bw=12 bwr=0.16 quickmatch k=14 idtag=t printunmappedcount ow=true qtrim=rl trimq=10 untrim null ref=TEMP_TAXA_1a1503228d98e2bf267bbb3872495_chloroMitoRiboRef.fa.gz pigz=t unpigz=t zl=6 in1=TEMP_MICROBE_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outu1=TEMP_CHLORO_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outm=filteredByTile.fq.gz scafstats=chloroStats.txt 



rm TEMP_TAXA_1a1503228d98e2bf267bbb3872495_chloroMitoRiboRef.fa.gz 



rm TEMP_MICROBE_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz 



bbsplit.sh ordered=false k=14 idtag=t usemodulo printunmappedcount ow=true qtrim=rl trimq=10 untrim kfilter=25 maxsites=1 tipsearch=0 minratio=.9 maxindel=3 minhits=2 bw=12 bwr=0.16 fast=true maxsites2=10 build=1 ef=0.03 bloomfilter bloomk=29 bloomhashes=1 bloomminhits=6 bloomserial outm=human.fq.gz path= refstats=refStats.txt pigz=t unpigz=t zl=9 forcereadonly in1=TEMP_CHLORO_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz outu1=12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz 



rm TEMP_CHLORO_1a1503228d98e2bf267bbb3872495_12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz 



bbmerge.sh loose overwrite=true in1=12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz ihist=ihist_merge.txt outc=cardinality.txt pigz=t unpigz=t zl=9 adapters=adaptersDetected.fa mininsert=25 ecct extend2=100 rem k=62 prefilter prealloc loglog 



kmercountexact.sh overwrite=true in1=12673.4.276583.AAGTCCGT-ACGGACTT.anqrpht.fastq.gz khist=khist.txt peaks=peaks.txt unpigz=t gchist 

