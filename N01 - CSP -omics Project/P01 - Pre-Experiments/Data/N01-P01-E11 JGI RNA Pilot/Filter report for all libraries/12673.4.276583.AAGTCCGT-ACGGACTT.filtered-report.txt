JGI RQC Filtering Report

Library:               CZGPG
Sequencing Project Id: 1207405
Input Sequence Unit:   12673.4.276583.AAGTCCGT-ACGGACTT.fastq.gz  (841.0 MB)
Filtered File Name:    12673.4.276583.AAGTCCGT-ACGGACTT.filter-RNA.fastq.gz  (315.6 MB)
Filtering Date:        2018-10-02 00:03


Metric          | Reads         | Reads Removed % | Bases              | Bases Trimmed % |
----------------|---------------|-----------------|--------------------|-----------------|
Input           |    12,796,820 |         0.0000% |      1,932,319,820 |         0.0000% | Initial reads and bases before filtering
Output          |    12,413,178 |         2.9979% |      1,811,559,015 |         6.2495% | Reads and bases remaining after filtering
----------------|---------------|-----------------|--------------------|-----------------|
Duplicate       |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that are duplicates [1]
Low Quality     |         2,994 |         0.0234% |            636,439 |         0.0329% | Reads or bases trimmed due to low quality or contained N's
Artifacts       |           172 |         0.0013% |             25,553 |         0.0013% | Reads removed that contained synthetic artifacts
Spike-in        |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that contained spike-in sequences
Force Trimmed   |             0 |         0.0000% |         12,796,820 |         0.6623% | Last base removed from 76bp, 101bp, 151bp reads [2]
Adapter         |       183,408 |         1.4332% |         78,721,048 |         4.0739% | Reads removed that contained synthetic adapters
Chloroplast     |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that mapped to Chloroplast
Mitochondria    |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that mapped to Mitochondria
Ribosomal RNA   |       196,426 |         1.5350% |         28,492,825 |         1.4745% | Reads removed that mapped to Ribosomal RNA
Human           |           364 |         0.0028% |             50,763 |         0.0026% | Reads removed that mapped to a human genome reference
Mouse           |            20 |         0.0002% |              2,632 |         0.0001% | Reads removed that mapped to a mouse genome reference
Cat             |             4 |         0.0000% |                600 |         0.0000% | Reads removed that mapped to a cat genome reference
Dog             |            10 |         0.0001% |              1,500 |         0.0001% | Reads removed that mapped to a dog genome reference
Microbe         |           244 |         0.0019% |             32,625 |         0.0017% | Reads removed that mapped to a non-synthetic contamination database
----------------|---------------|-----------------|--------------------|-----------------|
Total Removed   |       383,642 |         2.9979% |        120,760,805 |         6.2495% |

Filtering parameters:
  BBTools Version: 38.25
  rqc.filter2.sh \
  rna=t minlength=49 qtrim=r maq=10 trimq=6 trimfragadapter=t phix=t maxns=1 mlf=0.33 ribomap=t removehuman=t removedog=t removecat=t removemouse=t khist=t removemicrobes=t sketch kapa=t taxlist=27327 clumpify=t tmpdir=null barcodefilter=f trimpolyg=5


[1] Duplicates considered as paired reads that start and end with the same sequence and have at most one base mismatch that match other paired ended reads
[2] Last base removed due to low quality

The difference in file sizes is partly due to the clumpify part of the filtering process that groups similar reads together which allows for better gzip compression.

