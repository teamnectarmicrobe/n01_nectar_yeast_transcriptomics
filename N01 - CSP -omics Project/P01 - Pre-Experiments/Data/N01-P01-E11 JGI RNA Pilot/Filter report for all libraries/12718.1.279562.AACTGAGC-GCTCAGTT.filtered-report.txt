JGI RQC Filtering Report

Library:               CZYTO
Sequencing Project Id: 1207405
Input Sequence Unit:   12718.1.279562.AACTGAGC-GCTCAGTT.fastq.gz  (1.0 GB)
Filtered File Name:    12718.1.279562.AACTGAGC-GCTCAGTT.filter-RNA.fastq.gz  (439.1 MB)
Filtering Date:        2018-10-28 23:35


Metric          | Reads         | Reads Removed % | Bases              | Bases Trimmed % |
----------------|---------------|-----------------|--------------------|-----------------|
Input           |    21,962,832 |         0.0000% |      3,316,387,632 |         0.0000% | Initial reads and bases before filtering
Output          |    18,975,318 |        13.6026% |      2,813,968,359 |        15.1496% | Reads and bases remaining after filtering
----------------|---------------|-----------------|--------------------|-----------------|
Duplicate       |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that are duplicates [1]
Low Quality     |        60,752 |         0.2766% |         10,997,049 |         0.3316% | Reads or bases trimmed due to low quality or contained N's
Artifacts       |           278 |         0.0013% |             41,031 |         0.0012% | Reads removed that contained synthetic artifacts
Spike-in        |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that contained spike-in sequences
Force Trimmed   |             0 |         0.0000% |         21,962,832 |         0.6623% | Last base removed from 76bp, 101bp, 151bp reads [2]
Adapter         |       239,060 |         1.0885% |         69,513,364 |         2.0961% | Reads removed that contained synthetic adapters
Chloroplast     |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that mapped to Chloroplast
Mitochondria    |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that mapped to Mitochondria
Ribosomal RNA   |     2,686,956 |        12.2341% |        399,837,147 |        12.0564% | Reads removed that mapped to Ribosomal RNA
Human           |           362 |         0.0016% |             52,418 |         0.0016% | Reads removed that mapped to a human genome reference
Mouse           |             2 |         0.0000% |                254 |         0.0000% | Reads removed that mapped to a mouse genome reference
Cat             |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that mapped to a cat genome reference
Dog             |             0 |         0.0000% |                  0 |         0.0000% | Reads removed that mapped to a dog genome reference
Microbe         |           104 |         0.0005% |             15,178 |         0.0005% | Reads removed that mapped to a non-synthetic contamination database
----------------|---------------|-----------------|--------------------|-----------------|
Total Removed   |     2,987,514 |        13.6026% |        502,419,273 |        15.1496% |

Filtering parameters:
  BBTools Version: 38.26
  rqc.filter2.sh \
  rna=t minlength=49 qtrim=r maq=10 trimq=6 trimfragadapter=t phix=t maxns=1 mlf=0.33 ribomap=t removehuman=t removedog=t removecat=t removemouse=t khist=t removemicrobes=t sketch kapa=t taxlist=27327 clumpify=t barcodefilter=f trimpolyg=5


[1] Duplicates considered as paired reads that start and end with the same sequence and have at most one base mismatch that match other paired ended reads
[2] Last base removed due to low quality

The difference in file sizes is partly due to the clumpify part of the filtering process that groups similar reads together which allows for better gzip compression.

