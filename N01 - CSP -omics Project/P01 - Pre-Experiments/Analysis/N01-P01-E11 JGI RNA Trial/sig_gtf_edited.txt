sc0000001:432850-433404	augustus_masked-sc0000001-processed-gene-0.56-mRNA-1	transcript_id 	;
sc0000001:523289-524413	augustus_masked-sc0000001-processed-gene-1.462-mRNA-1	transcript_id 	;
sc0000001:642288-643007	augustus_masked-sc0000001-processed-gene-1.474-mRNA-1	transcript_id 	;
sc0000001:827924-828718	augustus_masked-sc0000001-processed-gene-1.426-mRNA-1	transcript_id 	;
sc0000001:830581-831732	snap_masked-sc0000001-processed-gene-1.56-mRNA-1	transcript_id 	;
sc0000001:831918-834101	augustus_masked-sc0000001-processed-gene-1.511-mRNA-1	transcript_id 	;
sc0000001:956495-957304	augustus_masked-sc0000001-processed-gene-1.537-mRNA-1	transcript_id 	;
sc0000001:1003898-1004101	augustus_masked-sc0000001-processed-gene-2.16-mRNA-1	transcript_id 	;
sc0000001:1199902-1201149	augustus_masked-sc0000001-processed-gene-2.52-mRNA-1	transcript_id 	;
sc0000001:1201574-1203397	augustus_masked-sc0000001-processed-gene-2.53-mRNA-1	transcript_id 	;
sc0000001:1239019-1239696	snap_masked-sc0000001-processed-gene-2.307-mRNA-1	transcript_id 	;
sc0000001:1252501-1253781	augustus_masked-sc0000001-processed-gene-2.152-mRNA-1	transcript_id 	;
sc0000001:1254039-1254405	genemark-sc0000001-processed-gene-2.504-mRNA-1	transcript_id 	;
sc0000001:1255548-1257970	genemark-sc0000001-processed-gene-2.504-mRNA-1	transcript_id 	;
sc0000001:1257999-1258001	genemark-sc0000001-processed-gene-2.504-mRNA-1	transcript_id 	;
sc0000001:1342991-1344124	snap_masked-sc0000001-processed-gene-2.324-mRNA-1	transcript_id 	;
sc0000001:1533289-1535598	augustus_masked-sc0000001-processed-gene-3.93-mRNA-1	transcript_id 	;
sc0000001:1871155-1872726	augustus_masked-sc0000001-processed-gene-3.62-mRNA-1	transcript_id 	;
sc0000001:2032221-2034506	augustus_masked-sc0000001-processed-gene-4.436-mRNA-1	transcript_id 	;
sc0000001:2136800-2137206	genemark-sc0000001-processed-gene-4.31-mRNA-1	transcript_id 	;
sc0000001:2137269-2138463	genemark-sc0000001-processed-gene-4.31-mRNA-1	transcript_id 	;
sc0000001:2138509-2138940	snap_masked-sc0000001-processed-gene-4.284-mRNA-1	transcript_id 	;
sc0000001:2196695-2197441	augustus_masked-sc0000001-processed-gene-4.463-mRNA-1	transcript_id 	;
sc0000001:2316766-2317338	augustus_masked-sc0000001-processed-gene-4.482-mRNA-1	transcript_id 	;
sc0000001:2354598-2357867	augustus_masked-sc0000001-processed-gene-4.490-mRNA-1	transcript_id 	;
sc0000001:2360046-2361461	snap_masked-sc0000001-processed-gene-4.247-mRNA-1	transcript_id 	;
sc0000001:2467551-2469098	snap_masked-sc0000001-processed-gene-4.263-mRNA-1	transcript_id 	;
sc0000001:2607146-2609317	augustus_masked-sc0000001-processed-gene-5.51-mRNA-1	transcript_id 	;
sc0000001:2707812-2708288	genemark-sc0000001-processed-gene-5.950-mRNA-1	transcript_id 	;
sc0000001:2749634-2750467	snap_masked-sc0000001-processed-gene-5.434-mRNA-1	transcript_id 	;
sc0000001:2818917-2819366	snap_masked-sc0000001-processed-gene-5.452-mRNA-1	transcript_id 	;
sc0000001:2819444-2821003	augustus_masked-sc0000001-processed-gene-5.274-mRNA-1	transcript_id 	;
sc0000001:2934348-2934650	snap_masked-sc0000001-processed-gene-5.474-mRNA-1	transcript_id 	;
sc0000001:3031108-3032196	snap_masked-sc0000001-processed-gene-5.646-mRNA-1	transcript_id 	;
sc0000001:3039133-3040287	snap_masked-sc0000001-processed-gene-5.647-mRNA-1	transcript_id 	;
sc0000001:3124310-3125119	snap_masked-sc0000001-processed-gene-5.513-mRNA-1	transcript_id 	;
sc0000001:3126099-3126392	augustus_masked-sc0000001-processed-gene-5.340-mRNA-1	transcript_id 	;
sc0000001:3239455-3241026	snap_masked-sc0000001-processed-gene-5.686-mRNA-1	transcript_id 	;
sc0000002:12177-12536	snap_masked-sc0000002-processed-gene-0.375-mRNA-1	transcript_id 	;
sc0000002:110592-112631	snap_masked-sc0000002-processed-gene-0.388-mRNA-1	transcript_id 	;
sc0000002:179909-180526	snap_masked-sc0000002-processed-gene-0.404-mRNA-1	transcript_id 	;
sc0000002:328218-328763	snap_masked-sc0000002-processed-gene-0.433-mRNA-1	transcript_id 	;
sc0000002:503293-505038	augustus_masked-sc0000002-processed-gene-1.19-mRNA-1	transcript_id 	;
sc0000002:505863-507968	genemark-sc0000002-processed-gene-1.378-mRNA-1	transcript_id 	;
sc0000002:572728-573453	snap_masked-sc0000002-processed-gene-1.214-mRNA-1	transcript_id 	;
sc0000002:575961-576467	augustus_masked-sc0000002-processed-gene-1.32-mRNA-1	transcript_id 	;
sc0000002:721873-722451	snap_masked-sc0000002-processed-gene-1.244-mRNA-1	transcript_id 	;
sc0000002:758984-760222	augustus_masked-sc0000002-processed-gene-1.156-mRNA-1	transcript_id 	;
sc0000002:823799-825376	augustus_masked-sc0000002-processed-gene-1.79-mRNA-1	transcript_id 	;
sc0000002:887845-888690	snap_masked-sc0000002-processed-gene-1.345-mRNA-1	transcript_id 	;
sc0000002:972645-975765	snap_masked-sc0000002-processed-gene-1.365-mRNA-1	transcript_id 	;
sc0000002:975950-977361	snap_masked-sc0000002-processed-gene-1.365-mRNA-1	transcript_id 	;
sc0000002:1092777-1093271	snap_masked-sc0000002-processed-gene-2.899-mRNA-1	transcript_id 	;
sc0000002:1343703-1343712	snap_masked-sc0000002-processed-gene-2.799-mRNA-1	transcript_id 	;
sc0000002:1344005-1344543	snap_masked-sc0000002-processed-gene-2.799-mRNA-1	transcript_id 	;
sc0000002:1443964-1445997	snap_masked-sc0000002-processed-gene-2.962-mRNA-1	transcript_id 	;
sc0000002:1447737-1449002	augustus_masked-sc0000002-processed-gene-2.240-mRNA-1	transcript_id 	;
sc0000002:1627993-1629669	snap_masked-sc0000002-processed-gene-2.990-mRNA-1	transcript_id 	;
sc0000002:1722909-1723973	snap_masked-sc0000002-processed-gene-2.1002-mRNA-1	transcript_id 	;
sc0000003:163325-163972	augustus_masked-sc0000003-processed-gene-0.29-mRNA-1	transcript_id 	;
sc0000003:235264-236640	snap_masked-sc0000003-processed-gene-0.537-mRNA-1	transcript_id 	;
sc0000003:238196-238690	augustus_masked-sc0000003-processed-gene-0.138-mRNA-1	transcript_id 	;
sc0000003:290609-292525	augustus_masked-sc0000003-processed-gene-0.54-mRNA-1	transcript_id 	;
sc0000003:292886-293887	snap_masked-sc0000003-processed-gene-0.463-mRNA-1	transcript_id 	;
sc0000003:318149-318865	snap_masked-sc0000003-processed-gene-0.468-mRNA-1	transcript_id 	;
sc0000003:380600-382726	augustus_masked-sc0000003-processed-gene-0.163-mRNA-1	transcript_id 	;
sc0000003:443194-444267	snap_masked-sc0000003-processed-gene-0.496-mRNA-1	transcript_id 	;
sc0000003:474023-475339	augustus_masked-sc0000003-processed-gene-0.179-mRNA-1	transcript_id 	;
sc0000003:523832-526564	augustus_masked-sc0000003-processed-gene-1.106-mRNA-1	transcript_id 	;
sc0000003:665852-667260	snap_masked-sc0000003-processed-gene-1.564-mRNA-1	transcript_id 	;
sc0000003:667475-667490	snap_masked-sc0000003-processed-gene-1.564-mRNA-1	transcript_id 	;
sc0000003:685219-686919	snap_masked-sc0000003-processed-gene-1.569-mRNA-1	transcript_id 	;
sc0000003:817173-818453	augustus_masked-sc0000003-processed-gene-1.166-mRNA-1	transcript_id 	;
sc0000003:861794-863314	snap_masked-sc0000003-processed-gene-1.520-mRNA-1	transcript_id 	;
sc0000003:1165046-1168288	snap_masked-sc0000003-processed-gene-2.834-mRNA-1	transcript_id 	;
sc0000003:1170063-1170329	snap_masked-sc0000003-processed-gene-2.673-mRNA-1	transcript_id 	;
sc0000003:1193950-1195434	augustus_masked-sc0000003-processed-gene-2.37-mRNA-1	transcript_id 	;
sc0000003:1239193-1239357	maker-sc0000003-snap-gene-2.313-mRNA-1	transcript_id 	;
sc0000003:1239565-1242498	maker-sc0000003-snap-gene-2.313-mRNA-1	transcript_id 	;
sc0000003:1509322-1511646	augustus_masked-sc0000003-processed-gene-2.108-mRNA-1	transcript_id 	;
sc0000003:1526179-1527819	genemark-sc0000003-processed-gene-2.462-mRNA-1	transcript_id 	;
sc0000004:177965-178522	genemark-sc0000004-processed-gene-0.42-mRNA-1	transcript_id 	;
sc0000004:215977-218310	augustus_masked-sc0000004-processed-gene-0.490-mRNA-1	transcript_id 	;
sc0000004:372383-374791	snap_masked-sc0000004-processed-gene-0.341-mRNA-1	transcript_id 	;
sc0000004:660429-660603	genemark-sc0000004-processed-gene-1.38-mRNA-1	transcript_id 	;
sc0000004:660690-661687	genemark-sc0000004-processed-gene-1.38-mRNA-1	transcript_id 	;
sc0000004:756696-756917	maker-sc0000004-snap-gene-1.555-mRNA-1	transcript_id 	;
sc0000004:756941-758416	maker-sc0000004-snap-gene-1.555-mRNA-1	transcript_id 	;
sc0000004:814246-815673	snap_masked-sc0000004-processed-gene-1.249-mRNA-1	transcript_id 	;
sc0000004:910180-912621	augustus_masked-sc0000004-processed-gene-1.441-mRNA-1	transcript_id 	;
sc0000004:969213-970001	snap_masked-sc0000004-processed-gene-1.275-mRNA-1	transcript_id 	;
sc0000004:1071064-1071312	augustus_masked-sc0000004-processed-gene-2.145-mRNA-1	transcript_id 	;
sc0000004:1216674-1219916	snap_masked-sc0000004-processed-gene-2.610-mRNA-1	transcript_id 	;
sc0000004:1255268-1255358	augustus_masked-sc0000004-processed-gene-2.186-mRNA-1	transcript_id 	;
sc0000004:1255577-1258122	augustus_masked-sc0000004-processed-gene-2.186-mRNA-1	transcript_id 	;
sc0000004:1279979-1281166	snap_masked-sc0000004-processed-gene-2.628-mRNA-1	transcript_id 	;
sc0000004:1281286-1284147	augustus_masked-sc0000004-processed-gene-2.189-mRNA-1	transcript_id 	;
sc0000004:1306747-1307580	snap_masked-sc0000004-processed-gene-2.742-mRNA-1	transcript_id 	;
sc0000004:1363674-1363880	snap_masked-sc0000004-processed-gene-2.748-mRNA-1	transcript_id 	;
sc0000004:1363895-1365832	snap_masked-sc0000004-processed-gene-2.748-mRNA-1	transcript_id 	;
sc0000004:1518087-1519361	genemark-sc0000004-processed-gene-2.542-mRNA-1	transcript_id 	;
sc0000004:1615811-1617121	augustus_masked-sc0000004-processed-gene-2.131-mRNA-1	transcript_id 	;
sc0000005:28855-30462	genemark-sc0000005-processed-gene-0.253-mRNA-1	transcript_id 	;
sc0000005:30495-30689	genemark-sc0000005-processed-gene-0.253-mRNA-1	transcript_id 	;
sc0000005:47861-48535	augustus_masked-sc0000005-processed-gene-0.399-mRNA-1	transcript_id 	;
sc0000005:134247-134798	augustus_masked-sc0000005-processed-gene-0.497-mRNA-1	transcript_id 	;
sc0000005:226635-229886	genemark-sc0000005-processed-gene-0.287-mRNA-1	transcript_id 	;
sc0000005:229944-229946	genemark-sc0000005-processed-gene-0.287-mRNA-1	transcript_id 	;
sc0000005:264099-265289	augustus_masked-sc0000005-processed-gene-0.440-mRNA-1	transcript_id 	;
sc0000005:377373-379190	augustus_masked-sc0000005-processed-gene-0.554-mRNA-1	transcript_id 	;
sc0000005:391667-392818	snap_masked-sc0000005-processed-gene-0.55-mRNA-1	transcript_id 	;
sc0000005:523157-524863	augustus_masked-sc0000005-processed-gene-1.652-mRNA-1	transcript_id 	;
sc0000005:526651-528291	snap_masked-sc0000005-processed-gene-1.295-mRNA-1	transcript_id 	;
sc0000005:572477-573838	snap_masked-sc0000005-processed-gene-1.302-mRNA-1	transcript_id 	;
sc0000005:574513-576336	augustus_masked-sc0000005-processed-gene-1.526-mRNA-1	transcript_id 	;
sc0000005:576470-577536	snap_masked-sc0000005-processed-gene-1.420-mRNA-1	transcript_id 	;
sc0000005:577675-577729	snap_masked-sc0000005-processed-gene-1.420-mRNA-1	transcript_id 	;
sc0000005:578164-579202	maker-sc0000005-snap-gene-1.806-mRNA-1	transcript_id 	;
sc0000005:579223-580565	maker-sc0000005-snap-gene-1.806-mRNA-1	transcript_id 	;
sc0000005:646679-646850	genemark-sc0000005-processed-gene-1.161-mRNA-1	transcript_id 	;
sc0000005:646916-647006	genemark-sc0000005-processed-gene-1.161-mRNA-1	transcript_id 	;
sc0000005:647048-647855	genemark-sc0000005-processed-gene-1.161-mRNA-1	transcript_id 	;
sc0000006:114327-115127	maker-sc0000006-snap-gene-0.630-mRNA-1	transcript_id 	;
sc0000006:115653-116657	maker-sc0000006-snap-gene-0.630-mRNA-1	transcript_id 	;
sc0000006:144197-148033	snap_masked-sc0000006-processed-gene-0.179-mRNA-1	transcript_id 	;
sc0000006:149441-150387	snap_masked-sc0000006-processed-gene-0.180-mRNA-1	transcript_id 	;
sc0000006:150525-150531	snap_masked-sc0000006-processed-gene-0.180-mRNA-1	transcript_id 	;
sc0000006:308121-308423	snap_masked-sc0000006-processed-gene-0.200-mRNA-1	transcript_id 	;
sc0000006:439904-441402	maker-sc0000006-snap-gene-0.664-mRNA-1	transcript_id 	;
sc0000006:442823-444358	maker-sc0000006-snap-gene-0.664-mRNA-1	transcript_id 	;
sc0000006:444587-444593	maker-sc0000006-snap-gene-0.664-mRNA-1	transcript_id 	;
sc0000006:662565-663680	augustus_masked-sc0000006-processed-gene-0.789-mRNA-1	transcript_id 	;
sc0000006:669874-671886	augustus_masked-sc0000006-processed-gene-0.946-mRNA-1	transcript_id 	;
sc0000006:911249-911581	maker-sc0000006-snap-gene-0.652-mRNA-1	transcript_id 	;
sc0000006:911671-912204	maker-sc0000006-snap-gene-0.652-mRNA-1	transcript_id 	;
sc0000006:923595-925262	augustus_masked-sc0000006-processed-gene-0.992-mRNA-1	transcript_id 	;
sc0000007:12014-13666	snap_masked-sc0000007-processed-gene-0.770-mRNA-1	transcript_id 	;
sc0000007:22820-25123	snap_masked-sc0000007-processed-gene-0.653-mRNA-1	transcript_id 	;
sc0000007:81666-83435	snap_masked-sc0000007-processed-gene-0.666-mRNA-1	transcript_id 	;
sc0000007:127851-128391	genemark-sc0000007-processed-gene-0.391-mRNA-1	transcript_id 	;
sc0000007:128552-129734	genemark-sc0000007-processed-gene-0.391-mRNA-1	transcript_id 	;
sc0000007:129767-129785	genemark-sc0000007-processed-gene-0.391-mRNA-1	transcript_id 	;
sc0000007:212427-212623	genemark-sc0000007-processed-gene-0.411-mRNA-1	transcript_id 	;
sc0000007:212644-213125	genemark-sc0000007-processed-gene-0.411-mRNA-1	transcript_id 	;
sc0000007:213146-213441	genemark-sc0000007-processed-gene-0.411-mRNA-1	transcript_id 	;
sc0000007:374284-376743	augustus_masked-sc0000007-processed-gene-0.84-mRNA-1	transcript_id 	;
sc0000007:533008-535119	augustus_masked-sc0000007-processed-gene-0.255-mRNA-1	transcript_id 	;
sc0000007:535635-538289	augustus_masked-sc0000007-processed-gene-0.256-mRNA-1	transcript_id 	;
sc0000007:594827-600244	augustus_masked-sc0000007-processed-gene-0.271-mRNA-1	transcript_id 	;
sc0000007:728176-729039	augustus_masked-sc0000007-processed-gene-0.136-mRNA-1	transcript_id 	;
sc0000007:729712-729999	augustus_masked-sc0000007-processed-gene-0.136-mRNA-1	transcript_id 	;
sc0000008:63169-64686	genemark-sc0000008-processed-gene-0.433-mRNA-1	transcript_id 	;
sc0000008:73679-74283	maker-sc0000008-snap-gene-0.620-mRNA-1	transcript_id 	;
sc0000008:74310-74466	maker-sc0000008-snap-gene-0.620-mRNA-1	transcript_id 	;
sc0000008:93273-94697	snap_masked-sc0000008-processed-gene-0.10-mRNA-1	transcript_id 	;
sc0000008:137548-138426	genemark-sc0000008-processed-gene-0.309-mRNA-1	transcript_id 	;
sc0000008:213948-214379	snap_masked-sc0000008-processed-gene-0.162-mRNA-1	transcript_id 	;
sc0000008:214400-214515	snap_masked-sc0000008-processed-gene-0.162-mRNA-1	transcript_id 	;
sc0000008:214741-215746	snap_masked-sc0000008-processed-gene-0.162-mRNA-1	transcript_id 	;
sc0000008:303038-303688	genemark-sc0000008-processed-gene-0.345-mRNA-1	transcript_id 	;
sc0000008:424615-424767	maker-sc0000008-snap-gene-0.601-mRNA-1	transcript_id 	;
sc0000008:425317-427131	maker-sc0000008-snap-gene-0.601-mRNA-1	transcript_id 	;
sc0000008:475754-476380	augustus_masked-sc0000008-processed-gene-0.731-mRNA-1	transcript_id 	;
sc0000008:509692-512370	snap_masked-sc0000008-processed-gene-0.228-mRNA-1	transcript_id 	;
sc0000008:618618-619361	snap_masked-sc0000008-processed-gene-0.104-mRNA-1	transcript_id 	;
sc0000008:645909-646707	genemark-sc0000008-processed-gene-0.412-mRNA-1	transcript_id 	;
sc0000008:646728-647062	genemark-sc0000008-processed-gene-0.412-mRNA-1	transcript_id 	;
sc0000009:55745-58225	snap_masked-sc0000009-processed-gene-0.135-mRNA-1	transcript_id 	;
sc0000009:101699-102835	snap_masked-sc0000009-processed-gene-0.12-mRNA-1	transcript_id 	;
sc0000009:161598-163481	augustus_masked-sc0000009-processed-gene-0.635-mRNA-1	transcript_id 	;
sc0000009:247580-248599	genemark-sc0000009-processed-gene-0.291-mRNA-1	transcript_id 	;
sc0000009:272344-273480	snap_masked-sc0000009-processed-gene-0.40-mRNA-1	transcript_id 	;
sc0000009:319359-320216	snap_masked-sc0000009-processed-gene-0.54-mRNA-1	transcript_id 	;
sc0000009:330073-333009	augustus_masked-sc0000009-processed-gene-0.675-mRNA-1	transcript_id 	;
sc0000009:347313-355022	augustus_masked-sc0000009-processed-gene-0.844-mRNA-1	transcript_id 	;
sc0000009:417367-417633	maker-sc0000009-snap-gene-0.575-mRNA-1	transcript_id 	;
sc0000009:418418-418705	maker-sc0000009-snap-gene-0.575-mRNA-1	transcript_id 	;
sc0000009:418719-418850	maker-sc0000009-snap-gene-0.575-mRNA-1	transcript_id 	;
sc0000009:523520-524392	snap_masked-sc0000009-processed-gene-0.234-mRNA-1	transcript_id 	;
sc0000009:535382-538237	genemark-sc0000009-processed-gene-0.361-mRNA-1	transcript_id 	;
sc0000009:593007-594818	snap_masked-sc0000009-processed-gene-0.104-mRNA-1	transcript_id 	;
sc0000009:642119-643519	snap_masked-sc0000009-processed-gene-0.248-mRNA-1	transcript_id 	;
sc0000010:10-88	genemark-sc0000010-processed-gene-0.413-mRNA-1	transcript_id 	;
sc0000010:163-1182	genemark-sc0000010-processed-gene-0.413-mRNA-1	transcript_id 	;
sc0000010:1203-1924	genemark-sc0000010-processed-gene-0.413-mRNA-1	transcript_id 	;
sc0000010:26448-28838	augustus_masked-sc0000010-processed-gene-0.40-mRNA-1	transcript_id 	;
sc0000010:70828-73005	snap_masked-sc0000010-processed-gene-0.326-mRNA-1	transcript_id 	;
sc0000010:109415-111337	maker-sc0000010-snap-gene-0.6-mRNA-1	transcript_id 	;
sc0000010:111376-111507	maker-sc0000010-snap-gene-0.6-mRNA-1	transcript_id 	;
sc0000010:210953-212314	augustus_masked-sc0000010-processed-gene-0.75-mRNA-1	transcript_id 	;
sc0000010:212511-213602	augustus_masked-sc0000010-processed-gene-0.76-mRNA-1	transcript_id 	;
sc0000010:282114-286376	augustus_masked-sc0000010-processed-gene-0.192-mRNA-1	transcript_id 	;
sc0000010:335776-338970	snap_masked-sc0000010-processed-gene-0.372-mRNA-1	transcript_id 	;
sc0000010:360741-361460	genemark-sc0000010-processed-gene-0.491-mRNA-1	transcript_id 	;
sc0000010:361505-361939	genemark-sc0000010-processed-gene-0.491-mRNA-1	transcript_id 	;
sc0000011:73350-74273	augustus_masked-sc0000011-processed-gene-0.321-mRNA-1	transcript_id 	;
sc0000011:177710-180400	snap_masked-sc0000011-processed-gene-0.20-mRNA-1	transcript_id 	;
sc0000011:182860-185082	snap_masked-sc0000011-processed-gene-0.21-mRNA-1	transcript_id 	;
sc0000012:6169-6822	snap_masked-sc0000012-processed-gene-0.0-mRNA-1	transcript_id 	;
sc0000012:12669-13124	snap_masked-sc0000012-processed-gene-0.67-mRNA-1	transcript_id 	;
sc0000012:28630-31314	augustus_masked-sc0000012-processed-gene-0.305-mRNA-1	transcript_id 	;
sc0000012:31557-34053	augustus_masked-sc0000012-processed-gene-0.306-mRNA-1	transcript_id 	;
sc0000012:34311-34456	augustus_masked-sc0000012-processed-gene-0.306-mRNA-1	transcript_id 	;
sc0000012:276896-278482	augustus_masked-sc0000012-processed-gene-0.431-mRNA-1	transcript_id 	;
sc0000012:284081-287098	augustus_masked-sc0000012-processed-gene-0.432-mRNA-1	transcript_id 	;
sc0000013:176697-178517	augustus_masked-sc0000013-processed-gene-0.34-mRNA-1	transcript_id 	;
sc0000013:190664-191233	augustus_masked-sc0000013-processed-gene-0.36-mRNA-1	transcript_id 	;
sc0000013:205885-206814	snap_masked-sc0000013-processed-gene-0.278-mRNA-1	transcript_id 	;
sc0000014:724-851	genemark-sc0000014-processed-gene-0.59-mRNA-1	transcript_id 	;
sc0000014:917-1034	genemark-sc0000014-processed-gene-0.59-mRNA-1	transcript_id 	;
sc0000014:1064-1161	genemark-sc0000014-processed-gene-0.59-mRNA-1	transcript_id 	;
sc0000014:1217-1603	genemark-sc0000014-processed-gene-0.59-mRNA-1	transcript_id 	;
sc0000014:1630-1810	genemark-sc0000014-processed-gene-0.59-mRNA-1	transcript_id 	;
sc0000014:1837-2298	genemark-sc0000014-processed-gene-0.59-mRNA-1	transcript_id 	;
sc0000014:2319-2890	genemark-sc0000014-processed-gene-0.59-mRNA-1	transcript_id 	;
sc0000014:2943-3555	genemark-sc0000014-processed-gene-0.59-mRNA-1	transcript_id 	;
sc0000014:47682-49253	genemark-sc0000014-processed-gene-0.7-mRNA-1	transcript_id 	;
sc0000014:64017-64925	snap_masked-sc0000014-processed-gene-0.193-mRNA-1	transcript_id 	;
sc0000014:202268-202954	genemark-sc0000014-processed-gene-0.100-mRNA-1	transcript_id 	;
sc0000014:266708-266765	snap_masked-sc0000014-processed-gene-0.172-mRNA-1	transcript_id 	;
sc0000014:266940-268153	snap_masked-sc0000014-processed-gene-0.172-mRNA-1	transcript_id 	;
sc0000014:269665-271391	augustus_masked-sc0000014-processed-gene-0.342-mRNA-1	transcript_id 	;
sc0000014:271787-271889	augustus_masked-sc0000014-processed-gene-0.342-mRNA-1	transcript_id 	;
sc0000014:277870-278349	snap_masked-sc0000014-processed-gene-0.175-mRNA-1	transcript_id 	;
sc0000015:18725-23239	augustus_masked-sc0000015-processed-gene-0.54-mRNA-1	transcript_id 	;
sc0000015:63089-63221	snap_masked-sc0000015-processed-gene-0.238-mRNA-1	transcript_id 	;
sc0000015:63299-63588	snap_masked-sc0000015-processed-gene-0.238-mRNA-1	transcript_id 	;
sc0000015:63767-64639	snap_masked-sc0000015-processed-gene-0.238-mRNA-1	transcript_id 	;
sc0000015:129811-130989	snap_masked-sc0000015-processed-gene-0.260-mRNA-1	transcript_id 	;
sc0000015:198112-199503	augustus_masked-sc0000015-processed-gene-0.49-mRNA-1	transcript_id 	;
sc0000015:199676-200131	augustus_masked-sc0000015-processed-gene-0.49-mRNA-1	transcript_id 	;
sc0000015:235055-235113	genemark-sc0000015-processed-gene-0.225-mRNA-1	transcript_id 	;
sc0000015:235135-235500	genemark-sc0000015-processed-gene-0.225-mRNA-1	transcript_id 	;
sc0000015:235527-235803	genemark-sc0000015-processed-gene-0.225-mRNA-1	transcript_id 	;
sc0000015:235827-236927	genemark-sc0000015-processed-gene-0.225-mRNA-1	transcript_id 	;
sc0000015:236953-237031	genemark-sc0000015-processed-gene-0.225-mRNA-1	transcript_id 	;
sc0000015:237077-237174	genemark-sc0000015-processed-gene-0.225-mRNA-1	transcript_id 	;
sc0000015:237253-237321	genemark-sc0000015-processed-gene-0.225-mRNA-1	transcript_id 	;
sc0000016:13037-13810	snap_masked-sc0000016-processed-gene-0.94-mRNA-1	transcript_id 	;
sc0000016:50049-51128	augustus_masked-sc0000016-processed-gene-0.11-mRNA-1	transcript_id 	;
sc0000017:12580-13362	augustus_masked-sc0000017-processed-gene-0.153-mRNA-1	transcript_id 	;
sc0000017:24240-25877	snap_masked-sc0000017-processed-gene-0.4-mRNA-1	transcript_id 	;
sc0000017:100415-102799	augustus_masked-sc0000017-processed-gene-0.200-mRNA-1	transcript_id 	;
sc0000017:159358-160545	augustus_masked-sc0000017-processed-gene-0.217-mRNA-1	transcript_id 	;
sc0000019:24205-24642	augustus_masked-sc0000019-processed-gene-0.71-mRNA-1	transcript_id 	;
sc0000019:90959-92344	snap_masked-sc0000019-processed-gene-0.16-mRNA-1	transcript_id 	;
sc0000019:92402-92419	snap_masked-sc0000019-processed-gene-0.16-mRNA-1	transcript_id 	;
sc0000021:29044-30624	augustus_masked-sc0000021-processed-gene-0.5-mRNA-1	transcript_id 	;
sc0000025:32310-32939	augustus_masked-sc0000025-processed-gene-0.12-mRNA-1	transcript_id 	;
sc0000025:35668-36002	snap_masked-sc0000025-processed-gene-0.52-mRNA-1	transcript_id 	;
sc0000025:36032-36038	snap_masked-sc0000025-processed-gene-0.52-mRNA-1	transcript_id 	;
sc0000026:39417-41345	snap_masked-sc0000026-processed-gene-0.36-mRNA-1	transcript_id 	;
sc0000035:10534-12186	snap_masked-sc0000035-processed-gene-0.17-mRNA-1	transcript_id 	;
sc0000035:35455-36249	augustus_masked-sc0000035-processed-gene-0.5-mRNA-1	transcript_id 	;
sc0000039:5388-5655	snap_masked-sc0000039-processed-gene-0.3-mRNA-1	transcript_id 	;
sc0000039:6094-6221	snap_masked-sc0000039-processed-gene-0.3-mRNA-1	transcript_id 	;
sc0000039:7370-7402	augustus_masked-sc0000039-processed-gene-0.20-mRNA-1	transcript_id 	;
sc0000039:7467-7817	augustus_masked-sc0000039-processed-gene-0.20-mRNA-1	transcript_id 	;
sc0000045:3-623	augustus_masked-sc0000045-processed-gene-0.14-mRNA-1	transcript_id 	;
sc0000045:6676-7983	augustus_masked-sc0000045-processed-gene-0.16-mRNA-1	transcript_id 	;
sc0000045:8114-9094	augustus_masked-sc0000045-processed-gene-0.17-mRNA-1	transcript_id 	;
sc0000045:9408-10994	augustus_masked-sc0000045-processed-gene-0.8-mRNA-1	transcript_id 	;
sc0000049:13299-15977	snap_masked-sc0000049-processed-gene-0.8-mRNA-1	transcript_id 	;
sc0000050:9242-9889	snap_masked-sc0000050-processed-gene-0.9-mRNA-1	transcript_id 	;
sc0000054:3747-4139	snap_masked-sc0000054-processed-gene-0.21-mRNA-1	transcript_id 	;
sc0000054:17571-18779	augustus_masked-sc0000054-processed-gene-0.9-mRNA-1	transcript_id 	;
sc0000054:19880-20179	maker-sc0000054-snap-gene-0.1-mRNA-1	transcript_id 	;
sc0000054:20190-20312	maker-sc0000054-snap-gene-0.1-mRNA-1	transcript_id 	;
sc0000055:19621-20376	snap_masked-sc0000055-processed-gene-0.27-mRNA-1	transcript_id 	;
sc0000056:17424-17861	snap_masked-sc0000056-processed-gene-0.18-mRNA-1	transcript_id 	;
sc0000058:31542-32101	maker-sc0000058-snap-gene-0.36-mRNA-1	transcript_id 	;
sc0000058:32300-32889	maker-sc0000058-snap-gene-0.36-mRNA-1	transcript_id 	;
sc0000058:33268-33599	maker-sc0000058-snap-gene-0.36-mRNA-1	transcript_id 	;
sc0000066:30820-31064	snap_masked-sc0000066-processed-gene-0.7-mRNA-1	transcript_id 	;
sc0000066:31146-31212	snap_masked-sc0000066-processed-gene-0.7-mRNA-1	transcript_id 	;
sc0000068:13306-15714	snap_masked-sc0000068-processed-gene-0.2-mRNA-1	transcript_id 	;
sc0000077:5974-6588	snap_masked-sc0000077-processed-gene-0.8-mRNA-1	transcript_id 	;
sc0000080:2183-2401	maker-sc0000080-snap-gene-0.1-mRNA-1	transcript_id 	;
sc0000080:2419-2527	maker-sc0000080-snap-gene-0.1-mRNA-1	transcript_id 	;
sc0000080:2943-3658	maker-sc0000080-snap-gene-0.1-mRNA-1	transcript_id 	;
sc0000080:3682-4149	maker-sc0000080-snap-gene-0.1-mRNA-1	transcript_id 	;
sc0000080:4161-4682	maker-sc0000080-snap-gene-0.1-mRNA-1	transcript_id 	;
sc0000081:20018-20903	genemark-sc0000081-processed-gene-0.17-mRNA-1	transcript_id 	;
sc0000081:20960-23853	genemark-sc0000081-processed-gene-0.17-mRNA-1	transcript_id 	;
sc0000082:8430-8436	genemark-sc0000082-processed-gene-0.13-mRNA-1	transcript_id 	;
sc0000082:8892-9307	genemark-sc0000082-processed-gene-0.13-mRNA-1	transcript_id 	;
sc0000086:8962-10239	augustus_masked-sc0000086-processed-gene-0.20-mRNA-1	transcript_id 	;
sc0000089:17465-17779	genemark-sc0000089-processed-gene-0.23-mRNA-1	transcript_id 	;
sc0000089:17894-17896	genemark-sc0000089-processed-gene-0.23-mRNA-1	transcript_id 	;
sc0000089:18795-19256	snap_masked-sc0000089-processed-gene-0.31-mRNA-1	transcript_id 	;
sc0000089:19394-20599	augustus_masked-sc0000089-processed-gene-0.9-mRNA-1	transcript_id 	;
sc0000089:25375-25505	maker-sc0000089-snap-gene-0.13-mRNA-1	transcript_id 	;
sc0000089:25678-26026	maker-sc0000089-snap-gene-0.13-mRNA-1	transcript_id 	;
sc0000089:26067-26207	maker-sc0000089-snap-gene-0.13-mRNA-1	transcript_id 	;
sc0000089:26242-26438	maker-sc0000089-snap-gene-0.13-mRNA-1	transcript_id 	;
sc0000089:26471-26519	maker-sc0000089-snap-gene-0.13-mRNA-1	transcript_id 	;
sc0000090:6826-9852	augustus_masked-sc0000090-processed-gene-0.28-mRNA-1	transcript_id 	;
sc0000096:22389-23978	augustus_masked-sc0000096-processed-gene-0.21-mRNA-1	transcript_id 	;
sc0000097:17812-18132	genemark-sc0000097-processed-gene-0.10-mRNA-1	transcript_id 	;
sc0000106:6629-7186	genemark-sc0000106-processed-gene-0.18-mRNA-1	transcript_id 	;
sc0000114:14160-14330	snap_masked-sc0000114-processed-gene-0.6-mRNA-1	transcript_id 	;
sc0000114:14369-14905	snap_masked-sc0000114-processed-gene-0.6-mRNA-1	transcript_id 	;
sc0000114:15093-15260	snap_masked-sc0000114-processed-gene-0.6-mRNA-1	transcript_id 	;
sc0000114:15308-15493	snap_masked-sc0000114-processed-gene-0.6-mRNA-1	transcript_id 	;
sc0000114:15570-15839	snap_masked-sc0000114-processed-gene-0.6-mRNA-1	transcript_id 	;
sc0000114:15884-16030	snap_masked-sc0000114-processed-gene-0.6-mRNA-1	transcript_id 	;
sc0000114:16139-16370	snap_masked-sc0000114-processed-gene-0.6-mRNA-1	transcript_id 	;
sc0000114:16472-16479	snap_masked-sc0000114-processed-gene-0.6-mRNA-1	transcript_id 	;
sc0000120:22-732	snap_masked-sc0000120-processed-gene-0.1-mRNA-1	transcript_id 	;