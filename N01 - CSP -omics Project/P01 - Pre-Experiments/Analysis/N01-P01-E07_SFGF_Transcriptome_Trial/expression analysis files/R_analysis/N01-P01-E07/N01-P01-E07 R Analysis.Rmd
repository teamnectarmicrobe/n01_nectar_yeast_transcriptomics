---
title: "N01-P01-E07 Analysis"
author: "Callie Chappell"
date: "6/2/2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
##Summary of Analysis: 


#1. Import libraries: 
```{r}
library(readr)
library(dplyr)
library(DESeq2)
library(tidyr)
```
Also, I installed the following libraries into R from the command line: 
source("https://bioconductor.org/biocLite.R")
biocLite("DESeq2") ; library(DESeq2)
biocLite("ggplot2") ; library(ggplot2)
biocLite("clusterProfiler") ; library(clusterProfiler)
biocLite("biomaRt") ; library(biomaRt)
biocLite("ReactomePA") ; library(ReactomePA)
biocLite("DOSE") ; library(DOSE)
biocLite("KEGG.db") ; library(KEGG.db)
biocLite("org.Sc.sgd.db"); library(org.Sc.sgd.db)
biocLite("org.Hs.eg.db") ; library(org.Hs.eg.db)
biocLite("pheatmap") ; library(pheatmap)
biocLite("genefilter") ; library(genefilter)
biocLite("RColorBrewer") ; library(RColorBrewer)
biocLite("GO.db") ; library(GO.db)
biocLite("topGO") ; library(topGO)
biocLite("dplyr") ; library(dplyr)
biocLite("gage") ; library(gage)
biocLite("ggsci") ; library(ggsci)
Source: https://github.com/twbattaglia/RNAseq-workflow/blob/master/README.Rmd 


##Analysis:
#I. Data import and pruning: 
1. Import data: 
-exp_df is the differential expression of the aligned genes to the reference genome (MR1_A10)
-blast_df are the blast results when these genes were blasted against S.cerevisae genone (S288C). The headers are BLASTn output format 6 (http://www.metagenomics.wiki/tools/blast/blastn-output-format-6) except "qseqid" was changed to "locus" to match the column in exp_df. 
```{r}
#Import expression file: 
#library(readr)
exp_df <- all_trts <- read_delim("all-trts.txt", "\t", 
    escape_double = FALSE, trim_ws = TRUE)

#change column names: 
colnames(exp_df) <- c("gene_id","locus","glu_10_low","glu_4_low","cas_10_high","cas_10_low","cas_4_high","cas_4_low")

#Import BLAST alignment file to S. cerevisae: 
blast_df <- read_delim("orf_output_dc_megablast.blast", 
    "\t", escape_double = FALSE, col_names = FALSE, 
    trim_ws = TRUE)
#Schuyler's BLAST results: 
#blast_df <- read_delim("~/Box Sync/Stanford/Lab Notebook/N01 - CSP -omics Project/P01 - Pre-Experiments/Analysis/N01-P01-E07_SFGF_Transcriptome_Trial/CC_transcriptome/out.blast", 
#    "\t", escape_double = FALSE, col_names = FALSE, 
#    trim_ws = TRUE)
#Add comprehensible column names to blast_df: 
colnames(blast_df) <- c("locus","sseqid"," pident","length","mismatch","gapopen","qstart","qend","sstart","send","evalue", "bitscore")
```

2. Append blast_df results to each gene in exp_df to produce "annotated" expression dataframe (aexp_df): 
```{r}
aexp_df <- merge(exp_df,blast_df, by  = "locus") 
```
A short commentary on this; of ~7000 expressed genes from RNAseq, only 145 of them aligned to any ORFs in the S. cerevisae genome. I'm going to move forward with this abridged analysis, but this might not accurately reflect what is going on, biologically. 

3. Make a simplifed dataframe with just the S.cerevisae gene name and expression level: 
```{r}
library(dplyr)
sexp_df <- aexp_df %>% select(sseqid,cas_10_high,cas_10_low,cas_4_high,cas_4_low)
```

5. Add GO terms: 
```{r}
#make vector of gene names: 
genes <- sexp_df[,1:1]
write.csv(matrix(as.character(genes),nrow=1),row.names = FALSE, file = "genes.csv")
#import csv from YeastMine: 
fun <- read_csv("~/Box Sync/Stanford/Lab Notebook/N01 - CSP -omics Project/P01 - Pre-Experiments/Analysis/N01-P01-E07_SFGF_Transcriptome_Trial/expression analysis files/functional_analysis/dc-megablast_results.csv", 
    col_names = FALSE)
#rename columns: 
colnames(fun) <- c("gene_id", "sseqid", "organism","gene_std_name", "gene", "gene_function_sum","gene_type","homologue","pathways_id","pathway_type","pathways_name", "pathways_description")
#only grab distinct rows: 
fun_unique <- fun[!duplicated(fun[,c('sseqid')]),]
#add descriptive names to sexp_df: 
sexp_df_2 <- merge(sexp_df, fun_unique, by="sseqid")
#only grab unique values with expression differences: 
sexp_df_2 <- sexp_df_2[!duplicated(sexp_df_2[,c('cas_10_high','cas_10_low','cas_4_high','cas_4_low')]),]

#add pathways info: 
pathways <- read_csv("pathways.csv")
sexp_df_2 <- merge(sexp_df_2, pathways, by="pathways_name")
```

#II. Differential Expression Analysis: 
1. Check differential expression with DESeq - since we only had 1 replicate, we can't really do this. 


#III. Visualization: 
1. PCA plot: 
```{r}
#generate PCA with just cas nectar at 10 hours (high and low N): 
#grab data: 
library(dplyr)
myvar <- c("sseqid","cas_10_high","cas_10_low")
pca_df <- aexp_df[myvar]
#reformat so genes are columns: 
#pca_df_spread <- spread(pca_df, sseqid, )
#rotate df to have 
plot(sexp_df_2$cas_10_high, sexp_df_2$cas_10_low)
pca <- prcomp(~cas_10_high + cas_10_low, data=sexp_df_2)
#look at results: 
#pca
#summary(pca)
#check scree plot: 
#plot(pca, type="l")
#look at rough plot: 
#biplot(pca, scale=0)

#check PCA scores: 
#str(pca)
#pca_df <- pca$x

#append PCA to sexp_df:
#pca_df <- cbind(sexp_df, pca$x)

#Let's plot in ggplot2, which is much nicer: 
#library(ggplot2)
#ggplot(pca_df, aes(PC1, PC2)) +
#  stat_ellipse(geom = 'polygon', col = 'black', alpha = 0.5) +
#  geom_point(shape = 21, col = "black")
```

2. Heatmap: 
```{r}
#make updated heatmap function for axis on other side: 
heatmap2 <- function (x, Rowv = NULL, Colv = if (symm) "Rowv" else NULL, 
    distfun = dist, hclustfun = hclust, reorderfun = function(d, 
        w) reorder(d, w), add.expr, symm = FALSE, revC = identical(Colv, 
        "Rowv"), scale = c("row", "column", "none"), na.rm = TRUE, 
    margins = c(5, 5), ColSideColors, RowSideColors, cexRow = 0.2 + 
        1/log10(nr), cexCol = 0.2 + 1/log10(nc), labRow = NULL, 
    labCol = NULL, main = NULL, xlab = NULL, ylab = NULL, keep.dendro = FALSE, 
    verbose = getOption("verbose"), ...) 
{
    scale <- if (symm && missing(scale)) 
        "none"
    else match.arg(scale)
    if (length(di <- dim(x)) != 2 || !is.numeric(x)) 
        stop("'x' must be a numeric matrix")
    nr <- di[1L]
    nc <- di[2L]
    if (nr <= 1 || nc <= 1) 
        stop("'x' must have at least 2 rows and 2 columns")
    if (!is.numeric(margins) || length(margins) != 2L) 
        stop("'margins' must be a numeric vector of length 2")
    doRdend <- !identical(Rowv, NA)
    doCdend <- !identical(Colv, NA)
    if (!doRdend && identical(Colv, "Rowv")) 
        doCdend <- FALSE
    if (is.null(Rowv)) 
        Rowv <- rowMeans(x, na.rm = na.rm)
    if (is.null(Colv)) 
        Colv <- colMeans(x, na.rm = na.rm)
    if (doRdend) {
        if (inherits(Rowv, "dendrogram")) 
            ddr <- Rowv
        else {
            hcr <- hclustfun(distfun(x))
            ddr <- as.dendrogram(hcr)
            if (!is.logical(Rowv) || Rowv) 
                ddr <- reorderfun(ddr, Rowv)
        }
        if (nr != length(rowInd <- order.dendrogram(ddr))) 
            stop("row dendrogram ordering gave index of wrong length")
    }
    else rowInd <- 1L:nr
    if (doCdend) {
        if (inherits(Colv, "dendrogram")) 
            ddc <- Colv
        else if (identical(Colv, "Rowv")) {
            if (nr != nc) 
                stop("Colv = \"Rowv\" but nrow(x) != ncol(x)")
            ddc <- ddr
        }
        else {
            hcc <- hclustfun(distfun(if (symm) 
                x
            else t(x)))
            ddc <- as.dendrogram(hcc)
            if (!is.logical(Colv) || Colv) 
                ddc <- reorderfun(ddc, Colv)
        }
        if (nc != length(colInd <- order.dendrogram(ddc))) 
            stop("column dendrogram ordering gave index of wrong length")
    }
    else colInd <- 1L:nc
    x <- x[rowInd, colInd]
    labRow <- if (is.null(labRow)) 
        if (is.null(rownames(x))) 
            (1L:nr)[rowInd]
        else rownames(x)
    else labRow[rowInd]
    labCol <- if (is.null(labCol)) 
        if (is.null(colnames(x))) 
            (1L:nc)[colInd]
        else colnames(x)
    else labCol[colInd]
    if (scale == "row") {
        x <- sweep(x, 1L, rowMeans(x, na.rm = na.rm), check.margin = FALSE)
        sx <- apply(x, 1L, sd, na.rm = na.rm)
        x <- sweep(x, 1L, sx, "/", check.margin = FALSE)
    }
    else if (scale == "column") {
        x <- sweep(x, 2L, colMeans(x, na.rm = na.rm), check.margin = FALSE)
        sx <- apply(x, 2L, sd, na.rm = na.rm)
        x <- sweep(x, 2L, sx, "/", check.margin = FALSE)
    }
    lmat <- rbind(c(NA, 3), 2:1)
    lwid <- c(if (doRdend) 1 else 0.05, 4)
    lhei <- c((if (doCdend) 1 else 0.05) + if (!is.null(main)) 0.2 else 0, 
        4)
    if (!missing(ColSideColors)) {
        if (!is.character(ColSideColors) || length(ColSideColors) != 
            nc) 
            stop("'ColSideColors' must be a character vector of length ncol(x)")
        lmat <- rbind(lmat[1, ] + 1, c(NA, 1), lmat[2, ] + 1)
        lhei <- c(lhei[1L], 0.2, lhei[2L])
    }
    if (!missing(RowSideColors)) {
        if (!is.character(RowSideColors) || length(RowSideColors) != 
            nr) 
            stop("'RowSideColors' must be a character vector of length nrow(x)")
        lmat <- cbind(lmat[, 1] + 1, c(rep(NA, nrow(lmat) - 1), 
            1), lmat[, 2] + 1)
        lwid <- c(lwid[1L], 0.2, lwid[2L])
    }
    lmat[is.na(lmat)] <- 0
    if (verbose) {
        cat("layout: widths = ", lwid, ", heights = ", lhei, 
            "; lmat=\n")
        print(lmat)
    }
    dev.hold()
    on.exit(dev.flush())
    op <- par(no.readonly = TRUE)
    on.exit(par(op), add = TRUE)
    layout(lmat, widths = lwid, heights = lhei, respect = TRUE)
    if (!missing(RowSideColors)) {
        par(mar = c(margins[1L], 0, 0, 0.5))
        image(rbind(if (revC) 
            nr:1L
        else 1L:nr), col = RowSideColors[rowInd], axes = FALSE)
    }
    if (!missing(ColSideColors)) {
        par(mar = c(0.5, 0, 0, margins[2L]))
        image(cbind(1L:nc), col = ColSideColors[colInd], axes = FALSE)
    }
    par(mar = c(margins[1L], 0, 0, margins[2L]))
    if (!symm || scale != "none") 
        x <- t(x)
    if (revC) {
        iy <- nr:1
        if (doRdend) 
            ddr <- rev(ddr)
        x <- x[, iy]
    }
    else iy <- 1L:nr
    image(1L:nc, 1L:nr, x, xlim = 0.5 + c(0, nc), ylim = 0.5 + 
        c(0, nr), axes = FALSE, xlab = "", ylab = "", ...)
    axis(1, 1L:nc, labels = labCol, las = 2, line = -0.5, tick = 0, 
        cex.axis = cexCol)
    if (!is.null(xlab)) 
        mtext(xlab, side = 1, line = margins[1L] - 1.25)
    axis(2, iy, labels = labRow, las = 2, line = -0.5, tick = 0, 
        cex.axis = cexRow)
    if (!is.null(ylab)) 
        mtext(ylab, side = 4, line = margins[2L] - 1.25)
    if (!missing(add.expr)) 
        eval.parent(substitute(add.expr))
    par(mar = c(margins[1L], 0, 0, 0))
    if (doRdend) 
        plot(ddr, horiz = TRUE, axes = FALSE, yaxs = "i", leaflab = "none")
    else frame()
    par(mar = c(0, 0, if (!is.null(main)) 1 else 0, margins[2L]))
    if (doCdend) 
        plot(ddc, axes = FALSE, xaxs = "i", leaflab = "none")
    else if (!is.null(main)) 
        frame()
    if (!is.null(main)) {
        par(xpd = NA)
        title(main, cex.main = 1.5 * op[["cex.main"]])
    }
    invisible(list(rowInd = rowInd, colInd = colInd, Rowv = if (keep.dendro && 
        doRdend) ddr, Colv = if (keep.dendro && doCdend) ddc))
}
library(gplots)


###PATHWAY
#sort by pathway 
sexp_df_3 <- sexp_df_2 %>% arrange(pathways_name)

#extract unique pathways and make new df with it for different colors: 
# library(RColorBrewer)
# n.pathways <- length(as.numeric(as.factor(unique(sexp_df_3$pathway_name))))
# palette <- rainbow(n.pathways)[sample(1:n.pathways, replace=F)]
# my_col <- palette[as.numeric(as.factor(sexp_df_3$pathways_name))]
# 
# #make heatmaps: 
# htmp_df <- sexp_df_3 
# 
# htmp=data.matrix(htmp_df[2:5])
# 
# heatmap(htmp,
#         Colv=NA, Rowv=NA,
#         labRow=sexp_df_3$gene_type,
#         #xlab="Treatment", ylab="Gene", main="Differential gene expression of M. reukaufii by N level", 
#         RowSideColors = my_col)
# 
# heatmap2(htmp,
#         Colv=NA, Rowv=NA,
#         labRow=sexp_df_3$pathways_name)

#export file: 
#write.csv(sexp_df_3, file = "exp_genes.csv")

###GROUPING
#sort by grouping 
sexp_df_4 <- sexp_df_2 %>% arrange(Biomolecule, grouping)

#extract unique pathways and make new df with it for different colors: 
library(RColorBrewer)
n.pathways <- length(as.numeric(as.factor(unique(sexp_df_4$pathway_name))))
palette <- rainbow(n.pathways)[sample(1:n.pathways, replace=F)]
my_col <- palette[as.numeric(as.factor(sexp_df_4$pathways_name))]

#make heatmaps: 
htmp_df <- sexp_df_4 

htmp=data.matrix(htmp_df[3:6])

heatmap2(htmp,
        Colv=NA, Rowv=NA,
        labRow=sexp_df_4$pathways_name,
        #xlab="Treatment", ylab="Gene", main="Differential gene expression of M. reukaufii by N level", 
        RowSideColors = my_col)

heatmap2(htmp,
        Colv=NA, Rowv=NA,
        labRow=sexp_df_4$grouping)

#export file: 
#write.csv(sexp_df_3, file = "exp_genes.csv")
```


3. Barchart of gene interactions: 
```{r}
library(ggplot2)

interactions <- read_delim("~/Box Sync/Stanford/Lab Notebook/N01 - CSP -omics Project/P01 - Pre-Experiments/Analysis/N01-P01-E07_SFGF_Transcriptome_Trial/expression analysis files/functional_analysis/interactions.tsv", 
    "\t", escape_double = FALSE, trim_ws = TRUE)

sm_int <- interactions %>% filter(Interactions>25)
colnames(sm_int) <- c("secondary_identifier","Gene","Interactions")
ggplot(data=sm_int, aes(x=reorder(Gene, Interactions), y=Interactions)) +
  geom_bar(stat = "identity", position = "dodge") +
  coord_flip() +
  labs(title="Gene Interactions", y="Number of interactions", x="Interacting genes")

#Why are these values off? 
```

4. Extract differences: 
```{r}
#extract genes where high > low: 
up_high_N <- sexp_df_3 %>% filter(cas_10_high>cas_10_low)
write.csv(up_high_N, file = "up_high_N.csv")
#extract genes where high < low: 
up_low_N <- sexp_df_3 %>% filter(cas_10_high<cas_10_low)
write.csv(up_low_N, file = "up_low_N.csv")
write.csv(sexp_df_3, file = "all_genes.csv")
```


5. Extract genes: 
```{r}
all_genes <- sexp_df$sseqid
write.csv(all_genes, file = "all_genes.csv")

pathways <- distinct(sexp_df_3, pathways_name)
write.csv(pathways, file = "pathways.csv")
```


 