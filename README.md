# N01_nectar_yeast_transcriptomics


## General overview

This project looks at the functional response of a population of nectar yeast M. reukaufii to nectar conditioned with nectar yeast M. rancensis. This repository is associated with the following pre-print: https://www.biorxiv.org/content/10.1101/2024.01.30.578081v1.full 

## Lab notebooks and code

The main directories here (P01-P07) contain code and data for all the main analyses. I tried to consolidate the key parts into these directories so you don't have to dig through ALL my code and notes. 

If you'd like to see that, the N01 - CSP -omics directory contanis ALL code and data for this project. 

If all else fails (or you prefer a nicer interface than git), all lab notebook entries and code can also be found in this Google Drive: https://drive.google.com/drive/folders/1ndplHyMwVRHarWo0boogEjx_jor2bA2u?usp=sharing. 

## Contact

If you have questions about this project, feel free to email Dr. Callie Chappell at calliech@stanford.edu. 

## Additonal data: 

- The .RData for N01-P05-E01 (DESeq2 analysis) is here: 10.6084/m9.figshare.25998346 
- Raw sequence reads are downloadable from the JGI Genome Portal (FD 1202157; SP 1202175; AP 1202158). 